#!/bin/bash

mkdir -p $GENIE/faser/Fluxes/Kling_2021

pushd $GENIE/faser/Fluxes/Kling_2021

# the original flux ntuple, small statistics
wget -O fullSampleReformat.root https://cernbox.cern.ch/s/2HZiFRz60gPYptY/download

#DPMJET high statistics
wget -O DPMJET.root https://cernbox.cern.ch/s/4OT9Dua4t0UsZjP/download

#SIBYLL high statistics
wget -O SIBYLL.root https://cernbox.cern.ch/s/yIrvMRThzSmvXnN/download

popd
