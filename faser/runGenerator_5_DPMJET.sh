#!/bin/bash

cd $GENIE_HOME/run

# instead of a fixed number of events, each job command generates 600 fb^-1 of neutrino interactions

gevgen_faser -l 600.0 -r 1 -g $GENIE/faser/Geometry/FaserNu3.gdml -f $GENIE/faser/Fluxes/Kling_2021/DPMJET.root --seed 23042801 --cross-sections $GENIE_HOME/faser_xsec/faserSplines.7TeV.xml -o faser3.600fbInv.DPMJET
#gevgen_faser -l 600.0 -r 1 -g $GENIE/faser/Geometry/FaserNu3.gdml -f $GENIE/faser/Fluxes/Kling_2021/SIBYLL.root --seed 23052501 --cross-sections $GENIE_HOME/faser_xsec/faserSplines.7TeV.xml -o faser3.600fbInv.SIBYLL

